__author__ = 'Jux'
# -*- coding: UTF-8 -*-
import logging, time

__metaclass__=type
class Logger:
    __time_stamp_day = time.strftime('%Y-%m-%d', time.localtime(time.time()))   # 生成时间戳，用于 log 文件名
    __log_format = '[%(asctime)s]\t[%(levelname)s]\n%(message)s'                # log 文件内部格式

    def __init__ (self):
        logging.basicConfig(format = self.__log_format, datefmt = '%H:%M:%S %p', level = 'DEBUG',
                            filename = self.__time_stamp_day + '.log', filemode = 'a')

    def WriteLog (self, log_detail):
        log_level = {'INFO': logging.info, 'DEBUG': logging.debug, 'ERROR': logging.error}
        log_level.get(log_detail.get('logLevel'))(log_detail.get('logMsg'))

if __name__=='__main__':
    pass