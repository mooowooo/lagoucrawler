__author__ = 'Jux'
# -*- coding: UTF-8 -*-
from bs4 import BeautifulSoup
import os, sys, urllib2, time, random


class Analiser(object):
    def __init__ (self, html):
        self.html = html

    def Analy (self, mark):
        soup = BeautifulSoup(self.html)
        list = soup.findAll(mark['name'], attrs = mark['attrs'])
        return list

    # def getLink (self, mark):


html = '''<!DOCTYPE HTML>
<html>
<head>
    <!-- <script id="allmobilize" charset="utf-8" src="http://www.lagou.com/js/third/allmobilize.min.js"></script> -->
    <script id="allmobilize" charset="utf-8"
            src="http://a.yunshipei.com/ef48a3377914af6ef846830fcae2a8e6/allmobilize.min.js"></script>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="alternate" media="handheld" href="#"/>
    <!-- end 云适配 -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>iOS招聘-招聘求职信息-拉勾网</title>
    <meta property="qc:admins" content="23635710066417756375"/>
    <meta content="iOS招聘  全国地区招聘 爱游戏(中国电信游戏基地)招聘iOS,月薪:6k-12k,要求:本科及以上学历,1-3年工作经验。职位诱惑：工作氛围和谐~正面激励成长~福利好~ 公司规模:150-500人移动互联网,游戏类公司招聘信息汇总  最新最热门互联网行业招聘信息，尽在拉勾网"
          name="description">
    <meta content="iOS招聘,全国地区iOS招聘,爱游戏(中国电信游戏基地)招聘iOS,移动互联网类公司招聘信息汇总,游戏类公司招聘信息汇总,拉勾招聘,拉勾网,互联网招聘" name="keywords">
    <meta name="baidu-site-verification" content="QIQ6KC1oZ6"/>

    <!-- <div class="web_root"  style="display:none">http://www.lagou.com</div> -->
    <script type="text/javascript">
        var ctx = "http://www.lagou.com";
        console.log(1);
    </script>
    <link rel="Shortcut Icon" href="http://www.lagou.com/images/favicon.ico">
    <link rel="stylesheet" type="text/css" href="http://www.lagou.com/css/style.css?v=1.5.5.5_0722_14"/>
    <link rel="stylesheet" type="text/css" href="http://www.lagou.com/css/external.min.css?v=1.5.5.5_0722_14"/>
    <link rel="stylesheet" type="text/css" href="http://www.lagou.com/css/popup.css?v=1.5.5.5_0722_14"/>
    <script src="http://www.lagou.com/js/libs/jquery.1.10.1.min.js?v=1.5.5.5_0722_14" type="text/javascript"></script>
    <script type="text/javascript" src="http://www.lagou.com/js/libs/jquery.lib.min.js?v=1.5.5.5_0722_14"></script>
    <script src="http://www.lagou.com/js/ajaxfileupload.js?v=1.5.5.5_0722_14" type="text/javascript"></script>
    <script type="text/javascript" src="http://www.lagou.com/js/additional-methods.js?v=1.5.5.5_0722_14"></script>
    <!--[if lte IE 8]>
    <script type="text/javascript" src="http://www.lagou.com/js/assets/excanvas.js?v=1.5.5.5_0722_14"></script>
    <![endif]-->
    <script type="text/javascript">
        var youdao_conv_id = 271546;
    </script>
    <script type="text/javascript" src="http://conv.youdao.com/pub/conv.js"></script>
</head>
<body>
<div id="body">
<div id="header">
    <div class="wrapper">
        <a href="http://www.lagou.com/" class="logo"></a>
        <ul class="reset" id="navheader">
            <li><a href="http://www.lagou.com/">首页</a></li>
            <li><a href="http://www.lagou.com/gongsi/">公司</a></li>
            <li><a href="http://www.lagou.com/toForum.html" target="_blank" rel="nofollow">论坛</a></li>
            <li><a href="http://www.lagou.com/resume/basic.html" rel="nofollow">我的简历</a></li>
        </ul>
        <dl class="collapsible_menu">
            <dt>
                <span id="nowrap">刘啸驹&nbsp;</span>
                <span id="noticeDot-0" class="red dn"></span>
                <i></i>
            </dt>
            <dd><a href="http://www.lagou.com/resume/myresume.html" rel="nofollow">我的简历</a></dd>
            <dd><a href="http://www.lagou.com/mycenter/collections.html">我收藏的职位</a></dd>
            <dd><a href="http://www.lagou.com/mycenter/delivery.html">我投递的职位 <span class="red dn"
                                                                                   id="noticeNo">(0)</span></a></dd>
            <dd class="btm"><a href="http://www.lagou.com/s/subscribe.html">我的订阅</a></dd>
            <dd><a href="http://www.lagou.com/mycenter/saveUserType.do?type=1">我要招人</a></dd>
            <dd><a href="http://www.lagou.com/user/accountBind.html">帐号设置</a></dd>
            <dd class="logout"><a href="http://www.lagou.com/user/logout.html" rel="nofollow">退出</a></dd>
        </dl>
        <div id="noticeTip" class="dn">
            <span class="bot"></span>
            <span class="top"></span>
            <a href="http://www.lagou.com/mycenter/delivery.html?tag=0&r=0" target="_blank"><strong>0</strong>条新投递反馈</a>
            <a href="javascript:;" class="closeNT"></a>
        </div>
    </div>
</div>
<!-- end #header -->
<div id="container">

<div class="sidebar">
    <div id="options" class="greybg">
        <dl>
            <dt>月薪范围 <em></em></dt>
            <dd>
                <div>2k以下</div>
                <div>2k-5k</div>
                <div>5k-10k</div>
                <div>10k-15k</div>
                <div>15k-25k</div>
                <div>25k-50k</div>
                <div>50k以上</div>
            </dd>
        </dl>
        <dl>
            <dt>工作经验 <em></em></dt>
            <dd>
                <div>不限</div>
                <div>应届毕业生</div>
                <div>1年以下</div>
                <div>1-3年</div>
                <div>3-5年</div>
                <div>5-10年</div>
                <div>10年以上</div>
            </dd>
        </dl>
        <dl>
            <dt>最低学历 <em></em></dt>
            <dd>
                <div>不限</div>
                <div>大专</div>
                <div>本科</div>
                <div>硕士</div>
                <div>博士</div>
            </dd>
        </dl>
        <dl>
            <dt>工作性质 <em></em></dt>
            <dd>
                <div>全职</div>
                <div>兼职</div>
                <div>实习</div>
            </dd>
        </dl>
        <dl>
            <dt>发布时间 <em></em></dt>
            <dd>
                <div>今天</div>
                <div>3天内</div>
                <div>一周内</div>
                <div>一月内</div>
            </dd>
        </dl>
    </div>

    <!-- QQ群 -->
    <div class="qq_ad" style="margin-bottom:20px;"></div>
    <!-- 广告位：主站搜索--QQ群 -->
    <script type="text/javascript">BAIDU_CLB_SLOT_ID = "946969";</script>
    <script type="text/javascript" src="http://cbjs.baidu.com/js/o.js"></script>

    <!-- 对外合作广告位  -->
    <!-- 对外合作广告位  -->

    <!-- 请置于所有广告位代码之前 -->
    <script type="text/javascript" src="http://cbjs.baidu.com/js/m.js"></script>
    <script type="text/javascript">
        BAIDU_CLB_preloadSlots("944593", "944592", "944591", "944590", "944589", "944588");
    </script>

    <!-- 广告位：搜索广告位-iOS1 -->
    <script type="text/javascript">BAIDU_CLB_fillSlot("944588");</script>

    <!-- 广告位：搜索广告位-iOS2 -->
    <script type="text/javascript">BAIDU_CLB_fillSlot("944589");</script>

    <!-- 广告位：搜索广告位-iOS3 -->
    <script type="text/javascript">BAIDU_CLB_fillSlot("944590");</script>

    <!-- 广告位：搜索广告位-iOS4 -->
    <script type="text/javascript">BAIDU_CLB_fillSlot("944591");</script>

    <!-- 广告位：搜索广告位-iOS5 -->
    <script type="text/javascript">BAIDU_CLB_fillSlot("944592");</script>

    <!-- 广告位：搜索广告位-iOS6 -->
    <script type="text/javascript">BAIDU_CLB_fillSlot("944593");</script>


    <!-- 请置于所有广告位代码之前 -->
    <script type="text/javascript" src="http://cbjs.baidu.com/js/m.js"></script>
    <script type="text/javascript">
        BAIDU_CLB_preloadSlots("944645", "944644", "944643", "944642", "944641", "944640");
    </script>

    <!-- 广告位：主站搜索广告位-其它1 -->
    <script type="text/javascript">BAIDU_CLB_fillSlot("944640");</script>

    <!-- 广告位：主站搜索广告位-其它2 -->
    <script type="text/javascript">BAIDU_CLB_fillSlot("944641");</script>

    <!-- 广告位：主站搜索广告位-其它3 -->
    <script type="text/javascript">BAIDU_CLB_fillSlot("944642");</script>

    <!-- 广告位：主站搜索广告位-其它4 -->
    <script type="text/javascript">BAIDU_CLB_fillSlot("944643");</script>

    <!-- 广告位：主站搜索广告位-其它5 -->
    <script type="text/javascript">BAIDU_CLB_fillSlot("944644");</script>

    <!-- 广告位：主站搜索广告位-其它6 -->
    <script type="text/javascript">BAIDU_CLB_fillSlot("944645");</script>


    <img src="http://www.lagou.com/images/partime_weixin.png" width="233" height="145" alt="找实习，扫我吧"
         class="partime_weixin"/>
</div>
<div class="content">
<div id="search_box">
    <form id="searchForm" name="searchForm" action="http://www.lagou.com/jobs/list_" method="get">
        <ul id="searchType">
            <li data-searchtype="1" class="type_selected">职位</li>
            <li data-searchtype="4">公司</li>
        </ul>
        <div class="searchtype_arrow"></div>
        <input type="text" id="search_input" name="kd" tabindex="1" value="iOS" placeholder="请输入职位名称，如：产品经理"/>
        <input type="hidden" name="spc" id="spcInput" value="1"/>
        <input type="hidden" name="pl" id="plInput" value=""/>
        <input type="hidden" name="gj" id="gjInput" value=""/>
        <input type="hidden" name="xl" id="xlInput" value=""/>
        <input type="hidden" name="yx" id="yxInput" value=""/>
        <input type="hidden" name="gx" id="gxInput" value=""/>
        <input type="hidden" name="st" id="stInput" value=""/>
        <input type="hidden" name="labelWords" id="labelWords" value="hot"/>
        <input type="hidden" name="lc" id="lc" value=""/>
        <input type="hidden" name="workAddress" id="workAddress" value=""/>
        <input type="hidden" name="city" id="cityInput" value="全国"/>
        <input type="hidden" name="requestId" id="requestId" value=""/>
        <input type="submit" id="search_button" value="搜索"/>
    </form>
    <input type="hidden" id="search_py" value="iOS"/>
</div>
<style>
    .ui-autocomplete {
        width: 488px;
        background: #fafafa !important;
        position: relative;
        z-index: 10;
        border: 2px solid #91cebe;
    }

    .ui-autocomplete-category {
        font-size: 16px;
        color: #999;
        width: 50px;
        position: absolute;
        z-index: 11;
        right: 0px; /*top: 6px; */
        text-align: center;
        border-top: 1px dashed #e5e5e5;
        padding: 5px 0;
    }

    .ui-menu-item {
        *width: 439px;
        vertical-align: middle;
        position: relative;
        margin: 0px;
        margin-right: 50px !important;
        background: #fff;
        border-right: 1px dashed #ededed;
    }

    .ui-menu-item a {
        display: block;
        overflow: hidden;
    }
</style>
<script type="text/javascript" src="http://www.lagou.com/js/search/search.min.js?v=1.5.5.5_0722_14"></script>
<dl class="hotSearch">
    <dt>热门搜索：</dt>
    <dd><a href="http://www.lagou.com/zhaopin/Java?labelWords=hot">Java</a></dd>
    <dd><a href="http://www.lagou.com/zhaopin/PHP?labelWords=hot">PHP</a></dd>
    <dd><a href="http://www.lagou.com/zhaopin/Android?labelWords=hot">Android</a></dd>
    <dd><a href="http://www.lagou.com/zhaopin/iOS?labelWords=hot">iOS</a></dd>
    <dd><a href="http://www.lagou.com/zhaopin/qianduan?labelWords=hot">前端</a></dd>
    <dd><a href="http://www.lagou.com/zhaopin/chanpinjingli1?labelWords=hot">产品经理</a></dd>
    <dd><a href="http://www.lagou.com/zhaopin/UI?labelWords=hot">UI</a></dd>
    <dd><a href="http://www.lagou.com/zhaopin/yunying1?labelWords=hot">运营</a></dd>
    <dd><a href="http://www.lagou.com/zhaopin/BD?labelWords=hot">BD</a></dd>
    <dd><a href="http://www.lagou.com/jobs/list_?gx=实习&city=全国">实习</a></dd>
</dl>
<div class="breakline"></div>
<dl class="workplace" id="workplaceSelect">
    <dt class="fl">工作地点：</dt>
    <dd>
        <a href="http://www.lagou.com/zhaopin/iOS" class="current">全国</a>
        |
    </dd>
    <dd>
        <a href="http://www.lagou.com/beijing-zhaopin/iOS">北京</a>
        |
    </dd>
    <dd>
        <a href="http://www.lagou.com/shanghai-zhaopin/iOS">上海</a>
        |
    </dd>
    <dd>
        <a href="http://www.lagou.com/guangzhou-zhaopin/iOS">广州</a>
        |
    </dd>
    <dd>
        <a href="http://www.lagou.com/shenzhen-zhaopin/iOS">深圳</a>
        |
    </dd>
    <dd>
        <a href="http://www.lagou.com/chengdu-zhaopin/iOS">成都</a>
        |
    </dd>
    <dd>
        <a href="http://www.lagou.com/hangzhou-zhaopin/iOS">杭州</a>
        |
    </dd>
    <dd>
        <a href="http://www.lagou.com/wuhan-zhaopin/iOS">武汉</a>
        |
    </dd>
    <dd>
        <a href="http://www.lagou.com/nanjing-zhaopin/iOS">南京</a>
        |
    </dd>
    <dd class="more">
        <a href="javascript:;">其他</a>

        <div class="triangle citymore_arrow"></div>
    </dd>
    <dd id="box_expectCity" class="searchlist_expectCity dn" data-cityjob="true">
        <span class="bot"></span>
        <span class="top"></span>
        <dl>
            <dt>ABCDEF</dt>
            <dd>
                <a href="http://www.lagou.com/beijing-zhaopin/iOS">北京</a>
                <a href="http://www.lagou.com/changchun-zhaopin/iOS">长春</a>
                <a href="http://www.lagou.com/chengdu-zhaopin/iOS">成都</a>
                <a href="http://www.lagou.com/chongqing-zhaopin/iOS">重庆</a>
                <a href="http://www.lagou.com/changsha-zhaopin/iOS">长沙</a>
                <a href="http://www.lagou.com/changshu-zhaopin/iOS">常熟</a>
                <a href="http://www.lagou.com/changzhou-zhaopin/iOS">常州</a>
                <a href="http://www.lagou.com/dongguan-zhaopin/iOS">东莞</a>
                <a href="http://www.lagou.com/dalian-zhaopin/iOS">大连</a>
                <a href="http://www.lagou.com/foshan-zhaopin/iOS">佛山</a>
                <a href="http://www.lagou.com/fuzhou-zhaopin/iOS">福州</a>
            </dd>
        </dl>
        <dl>
            <dt>GHIJ</dt>
            <dd>
                <a href="http://www.lagou.com/guiyang-zhaopin/iOS">贵阳</a>
                <a href="http://www.lagou.com/guangzhou-zhaopin/iOS">广州</a>
                <a href="http://www.lagou.com/haerbin-zhaopin/iOS">哈尔滨</a>
                <a href="http://www.lagou.com/hefei-zhaopin/iOS">合肥</a>
                <a href="http://www.lagou.com/haikou-zhaopin/iOS">海口</a>
                <a href="http://www.lagou.com/hangzhou-zhaopin/iOS">杭州</a>
                <a href="http://www.lagou.com/huizhou-zhaopin/iOS">惠州</a>
                <a href="http://www.lagou.com/jinhua-zhaopin/iOS">金华</a>
                <a href="http://www.lagou.com/jinan-zhaopin/iOS">济南</a>
                <a href="http://www.lagou.com/jiaxing-zhaopin/iOS">嘉兴</a>
            </dd>
        </dl>
        <dl>
            <dt>KLMN</dt>
            <dd>
                <a href="http://www.lagou.com/kunming-zhaopin/iOS">昆明</a>
                <a href="http://www.lagou.com/kunshan-zhaopin/iOS">昆山</a>
                <a href="http://www.lagou.com/langfang-zhaopin/iOS">廊坊</a>
                <a href="http://www.lagou.com/lishui-zhaopin/iOS">丽水</a>
                <a href="http://www.lagou.com/linyi-zhaopin/iOS">临沂</a>
                <a href="http://www.lagou.com/lanzhou-zhaopin/iOS">兰州</a>
                <a href="http://www.lagou.com/ningbo-zhaopin/iOS">宁波</a>
                <a href="http://www.lagou.com/nanchang-zhaopin/iOS">南昌</a>
                <a href="http://www.lagou.com/nanjing-zhaopin/iOS">南京</a>
                <a href="http://www.lagou.com/nanning-zhaopin/iOS">南宁</a>
            </dd>
        </dl>
        <dl>
            <dt>OPQR</dt>
            <dd>
                <a href="http://www.lagou.com/qingdao-zhaopin/iOS">青岛</a>
                <a href="http://www.lagou.com/quanzhou-zhaopin/iOS">泉州</a>
            </dd>
        </dl>
        <dl>
            <dt>STUV</dt>
            <dd>
                <a href="http://www.lagou.com/shanghai-zhaopin/iOS">上海</a>
                <a href="http://www.lagou.com/shijiazhuang-zhaopin/iOS">石家庄</a>
                <a href="http://www.lagou.com/shantou-zhaopin/iOS">汕头</a>
                <a href="http://www.lagou.com/shaoxing-zhaopin/iOS">绍兴</a>
                <a href="http://www.lagou.com/shenyang-zhaopin/iOS">沈阳</a>
                <a href="http://www.lagou.com/shenzhen-zhaopin/iOS">深圳</a>
                <a href="http://www.lagou.com/suzhou-zhaopin/iOS">苏州</a>
                <a href="http://www.lagou.com/tianjin-zhaopin/iOS">天津</a>
                <a href="http://www.lagou.com/taiyuan-zhaopin/iOS">太原</a>
                <a href="http://www.lagou.com/taizhou1-zhaopin/iOS">台州</a>
            </dd>
        </dl>
        <dl>
            <dt>WXYZ</dt>
            <dd>
                <a href="http://www.lagou.com/weifang-zhaopin/iOS">潍坊</a>
                <a href="http://www.lagou.com/wuhan-zhaopin/iOS">武汉</a>
                <a href="http://www.lagou.com/wulumuqi-zhaopin/iOS">乌鲁木齐</a>
                <a href="http://www.lagou.com/wuxi-zhaopin/iOS">无锡</a>
                <a href="http://www.lagou.com/wenzhou-zhaopin/iOS">温州</a>
                <a href="http://www.lagou.com/xian-zhaopin/iOS">西安</a>
                <a href="http://www.lagou.com/xiamen-zhaopin/iOS">厦门</a>
                <a href="http://www.lagou.com/xuzhou-zhaopin/iOS">徐州</a>
                <a href="http://www.lagou.com/yantai-zhaopin/iOS">烟台</a>
                <a href="http://www.lagou.com/zhuhai-zhaopin/iOS">珠海</a>
                <a href="http://www.lagou.com/zhenjiang-zhaopin/iOS">镇江</a>
                <a href="http://www.lagou.com/zhanjiang-zhaopin/iOS">湛江</a>
                <a href="http://www.lagou.com/zhaoqing-zhaopin/iOS">肇庆</a>
                <a href="http://www.lagou.com/zhongshan-zhaopin/iOS">中山</a>
                <a href="http://www.lagou.com/zhengzhou-zhaopin/iOS">郑州</a>
            </dd>
        </dl>
    </dd>
</dl>

<div id="tip_didi" class="dn">
    <span>亲，“嘀嘀打车”已更名为“滴滴打车”了哦，我们已帮您自动跳转~</span>
    <a href="javascript:;">我知道了</a>
</div>

<ul class="hot_pos reset">
<li class="odd clearfix">
    <div class="hot_pos_l">
        <div class="mb10">
            <a href="http://www.lagou.com/jobs/125343.html?source=search" title="iOS" target="_blank">iOS</a>
            &nbsp;
            <span class="c9">[南京]</span>
        </div>
        <span><em class="c7">月薪：</em>6k-12k</span>
        <span><em class="c7">经验：</em> 1-3年</span>
        <span><em class="c7">最低学历： </em>本科</span>
        <br/>
        <span><em class="c7">职位诱惑：</em>工作氛围和谐~正面激励成长~福利好~</span>
        <br/>
        <span>11:26发布</span>
    </div>
    <div class="hot_pos_r">
        <div class="apply">
            <a href="http://www.lagou.com/jobs/125343.html?source=search" target="_blank">投个简历</a>
        </div>
        <div class="mb10"><a href="http://www.lagou.com/gongsi/8307.html" title="爱游戏(中国电信游戏基地)" target="_blank">爱游戏(中国电信游戏基地)</a>
        </div>
        <span><em class="c7">领域： </em>移动互联网,游戏</span>
        <br/>
        <span><em class="c7">阶段： </em>初创型(未融资)</span>
        <span><em class="c7">规模： </em>150-500人</span>
        <ul class="companyTags reset">
            <li>绩效奖金</li>
            <li>年底双薪</li>
            <li>五险一金</li>
        </ul>
    </div>
</li>

<li class="clearfix">
    <div class="hot_pos_l">
        <div class="mb10">
            <a href="http://www.lagou.com/jobs/200276.html?source=search" title="iOS" target="_blank">iOS</a>
            &nbsp;
            <span class="c9">[北京]</span>
        </div>
        <span><em class="c7">月薪：</em>10k-20k</span>
        <span><em class="c7">经验：</em> 3-5年</span>
        <span><em class="c7">最低学历： </em>本科</span>
        <br/>
        <span><em class="c7">职位诱惑：</em>有竞争力的薪酬福利；舒适的办公环境</span>
        <br/>
        <span>10:32发布</span>
    </div>
    <div class="hot_pos_r">
        <div class="apply">
            <a href="http://www.lagou.com/jobs/200276.html?source=search" target="_blank">投个简历</a>
        </div>
        <div class="mb10"><a href="http://www.lagou.com/gongsi/2768.html" title="易到用车" target="_blank">易到用车</a></div>
        <span><em class="c7">领域： </em>移动互联网,O2O</span>
        <span><em class="c7">创始人：</em> 周航</span>
        <br/>
        <span><em class="c7">阶段： </em>成熟型(D轮及以上)</span>
        <span><em class="c7">规模： </em>150-500人</span>
        <ul class="companyTags reset">
            <li>绩效奖金</li>
            <li>股票期权</li>
            <li>五险一金</li>
        </ul>
    </div>
</li>

<li class="odd clearfix">
    <div class="hot_pos_l">
        <div class="mb10">
            <a href="http://www.lagou.com/jobs/84177.html?source=search" title="iOS" target="_blank">iOS</a>
            &nbsp;
            <span class="c9">[北京]</span>
        </div>
        <span><em class="c7">月薪：</em>18k-25k</span>
        <span><em class="c7">经验：</em> 3-5年</span>
        <span><em class="c7">最低学历： </em>本科</span>
        <br/>
        <span><em class="c7">职位诱惑：</em>借移动医疗大势享受坐直升飞机的职场发展</span>
        <br/>
        <span>09:57发布</span>
    </div>
    <div class="hot_pos_r">
        <div class="apply">
            <a href="http://www.lagou.com/jobs/84177.html?source=search" target="_blank">投个简历</a>
        </div>
        <div class="mb10"><a href="http://www.lagou.com/gongsi/1712.html" title="紫色医疗" target="_blank">紫色医疗</a></div>
        <span><em class="c7">领域： </em>移动互联网 ,健康医疗</span>
        <span><em class="c7">创始人：</em> Lu</span>
        <br/>
        <span><em class="c7">阶段： </em>成长型(A轮)</span>
        <span><em class="c7">规模： </em>15-50人</span>
        <ul class="companyTags reset">
            <li>五险一金</li>
            <li>股票期权</li>
            <li>年度旅游</li>
        </ul>
    </div>
</li>

<li class="clearfix">
    <div class="hot_pos_l">
        <div class="mb10">
            <a href="http://www.lagou.com/jobs/69909.html?source=search" title="iOS" target="_blank">iOS</a>
            &nbsp;
            <span class="c9">[上海]</span>
        </div>
        <span><em class="c7">月薪：</em>6k-12k</span>
        <span><em class="c7">经验：</em> 1-3年</span>
        <span><em class="c7">最低学历： </em>大专</span>
        <br/>
        <span><em class="c7">职位诱惑：</em>软萌妹纸多 老板好 发挥空间大 同龄人多</span>
        <br/>
        <span>3天前发布</span>
    </div>
    <div class="hot_pos_r">
        <div class="apply">
            <a href="http://www.lagou.com/jobs/69909.html?source=search" target="_blank">投个简历</a>
        </div>
        <div class="mb10"><a href="http://www.lagou.com/gongsi/12285.html" title="泰笛" target="_blank">泰笛</a></div>
        <span><em class="c7">领域： </em>电子商务</span>
        <span><em class="c7">创始人：</em> 姚宗场</span>
        <br/>
        <span><em class="c7">阶段： </em>成长型(B轮)</span>
        <span><em class="c7">规模： </em>50-150人</span>
        <ul class="companyTags reset">
            <li>绩效奖金</li>
            <li>专项奖金</li>
            <li>五险一金</li>
        </ul>
    </div>
</li>

<li class="odd clearfix">
    <div class="hot_pos_l">
        <div class="mb10">
            <a href="http://www.lagou.com/jobs/182490.html?source=search" title="iOS" target="_blank">iOS</a>
            &nbsp;
            <span class="c9">[北京]</span>
        </div>
        <span><em class="c7">月薪：</em>10k-20k</span>
        <span><em class="c7">经验：</em> 3-5年</span>
        <span><em class="c7">最低学历： </em>本科</span>
        <br/>
        <span><em class="c7">职位诱惑：</em>年底双薪，过节福利</span>
        <br/>
        <span>2014-08-20</span>
    </div>
    <div class="hot_pos_r">
        <div class="apply">
            <a href="http://www.lagou.com/jobs/182490.html?source=search" target="_blank">投个简历</a>
        </div>
        <div class="mb10"><a href="http://www.lagou.com/gongsi/608.html" title="拉手网" target="_blank">拉手网</a></div>
        <span><em class="c7">领域： </em>移动互联网,电子商务</span>
        <br/>
        <span><em class="c7">阶段： </em>成熟型(C轮)</span>
        <span><em class="c7">规模： </em>2000人以上</span>
        <ul class="companyTags reset">
        </ul>
    </div>
</li>

<li class="clearfix">
    <div class="hot_pos_l">
        <div class="mb10">
            <a href="http://www.lagou.com/jobs/11402.html?source=search" title="iOS" target="_blank">iOS</a>
            &nbsp;
            <span class="c9">[上海]</span>
        </div>
        <span><em class="c7">月薪：</em>10k-18k</span>
        <span><em class="c7">经验：</em> 不限</span>
        <span><em class="c7">最低学历： </em>本科</span>
        <br/>
        <span><em class="c7">职位诱惑：</em>薪资丰厚，文化开放，福利很好</span>
        <br/>
        <span>1天前发布</span>
    </div>
    <div class="hot_pos_r">
        <div class="apply">
            <a href="http://www.lagou.com/jobs/11402.html?source=search" target="_blank">投个简历</a>
        </div>
        <div class="mb10"><a href="http://www.lagou.com/gongsi/2750.html" title="iLegendSoft" target="_blank">iLegendSoft</a>
        </div>
        <span><em class="c7">领域： </em>移动互联网,社交</span>
        <span><em class="c7">创始人：</em> 郑济群</span>
        <br/>
        <span><em class="c7">阶段： </em>初创型(未融资)</span>
        <span><em class="c7">规模： </em>50-150人</span>
        <ul class="companyTags reset">
            <li>办公区域宽敞</li>
            <li>技能培训</li>
            <li>年底双薪</li>
        </ul>
    </div>
</li>

<li class="odd clearfix">
    <div class="hot_pos_l">
        <div class="mb10">
            <a href="http://www.lagou.com/jobs/117863.html?source=search" title="ios" target="_blank">ios</a>
            &nbsp;
            <span class="c9">[北京]</span>
        </div>
        <span><em class="c7">月薪：</em>10k-20k</span>
        <span><em class="c7">经验：</em> 1-3年</span>
        <span><em class="c7">最低学历： </em>本科</span>
        <br/>
        <span><em class="c7">职位诱惑：</em>弹性工作、五险一金、每年发14个月工资</span>
        <br/>
        <span>2014-08-20</span>
    </div>
    <div class="hot_pos_r">
        <div class="apply">
            <a href="http://www.lagou.com/jobs/117863.html?source=search" target="_blank">投个简历</a>
        </div>
        <div class="mb10"><a href="http://www.lagou.com/gongsi/21027.html" title="趣乐科技（智能钢琴,电子乐谱）" target="_blank">趣乐科技（智能钢琴,电子乐谱）</a>
        </div>
        <span><em class="c7">领域： </em>移动互联网,智能家居</span>
        <span><em class="c7">创始人：</em> 王正盛</span>
        <br/>
        <span><em class="c7">阶段： </em>成长型(A轮)</span>
        <span><em class="c7">规模： </em>15-50人</span>
        <ul class="companyTags reset">
            <li>腾讯体系</li>
            <li>行业前景好</li>
            <li>股票期权</li>
        </ul>
    </div>
</li>

<li class="clearfix">
    <div class="hot_pos_l">
        <div class="mb10">
            <a href="http://www.lagou.com/jobs/111715.html?source=search" title="iOS" target="_blank">iOS</a>
            &nbsp;
            <span class="c9">[上海]</span>
        </div>
        <span><em class="c7">月薪：</em>10k-15k</span>
        <span><em class="c7">经验：</em> 3-5年</span>
        <span><em class="c7">最低学历： </em>大专</span>
        <br/>
        <span><em class="c7">职位诱惑：</em>潜力大，成长快，资本雄厚，伯乐BOSS</span>
        <br/>
        <span>10:45发布</span>
    </div>
    <div class="hot_pos_r">
        <div class="apply">
            <a href="http://www.lagou.com/jobs/111715.html?source=search" target="_blank">投个简历</a>
        </div>
        <div class="mb10"><a href="http://www.lagou.com/gongsi/2460.html" title="汇联皆景" target="_blank">汇联皆景</a></div>
        <span><em class="c7">领域： </em>在线旅游</span>
        <span><em class="c7">创始人：</em> 谢晓</span>
        <br/>
        <span><em class="c7">阶段： </em>成长型(B轮)</span>
        <span><em class="c7">规模： </em>150-500人</span>
        <ul class="companyTags reset">
            <li>绩效奖金</li>
            <li>带薪年假</li>
            <li>弹性工作</li>
        </ul>
    </div>
</li>

<li class="odd clearfix">
    <div class="hot_pos_l">
        <div class="mb10">
            <a href="http://www.lagou.com/jobs/27356.html?source=search" title="iOS" target="_blank">iOS</a>
            &nbsp;
            <span class="c9">[深圳]</span>
        </div>
        <span><em class="c7">月薪：</em>16k-30k</span>
        <span><em class="c7">经验：</em> 3-5年</span>
        <span><em class="c7">最低学历： </em>本科</span>
        <br/>
        <span><em class="c7">职位诱惑：</em>高速发展 灵活主动 市场先驱 人才精英</span>
        <br/>
        <span>10:25发布</span>
    </div>
    <div class="hot_pos_r">
        <div class="apply">
            <a href="http://www.lagou.com/jobs/27356.html?source=search" target="_blank">投个简历</a>
        </div>
        <div class="mb10"><a href="http://www.lagou.com/gongsi/1720.html" title="深圳万兴信息科技股份有限公司" target="_blank">深圳万兴信息科技股份有限公司</a>
        </div>
        <span><em class="c7">领域： </em>移动互联网,智能家居</span>
        <span><em class="c7">创始人：</em> 吴太兵</span>
        <br/>
        <span><em class="c7">阶段： </em>成熟型(C轮)</span>
        <span><em class="c7">规模： </em>150-500人</span>
        <ul class="companyTags reset">
            <li>专项奖金</li>
            <li>股票期权</li>
            <li>五险一金</li>
        </ul>
    </div>
</li>

<li class="clearfix">
    <div class="hot_pos_l">
        <div class="mb10">
            <a href="http://www.lagou.com/jobs/74015.html?source=search" title="iOS" target="_blank">iOS</a>
            &nbsp;
            <span class="c9">[广州]</span>
        </div>
        <span><em class="c7">月薪：</em>8k-15k</span>
        <span><em class="c7">经验：</em> 1-3年</span>
        <span><em class="c7">最低学历： </em>本科</span>
        <br/>
        <span><em class="c7">职位诱惑：</em>弹性工作时间，扁平化管理，高福利报酬</span>
        <br/>
        <span>2014-08-20</span>
    </div>
    <div class="hot_pos_r">
        <div class="apply">
            <a href="http://www.lagou.com/jobs/74015.html?source=search" target="_blank">投个简历</a>
        </div>
        <div class="mb10"><a href="http://www.lagou.com/gongsi/15223.html" title="汇鼎品牌" target="_blank">汇鼎品牌</a></div>
        <span><em class="c7">领域： </em>移动互联网,电子商务</span>
        <span><em class="c7">创始人：</em> 侯国楠</span>
        <br/>
        <span><em class="c7">阶段： </em>初创型(天使轮)</span>
        <span><em class="c7">规模： </em>15-50人</span>
        <ul class="companyTags reset">
            <li>股票期权</li>
            <li>年终分红</li>
            <li>扁平管理</li>
        </ul>
    </div>
</li>

<li class="odd clearfix">
    <div class="hot_pos_l">
        <div class="mb10">
            <a href="http://www.lagou.com/jobs/103591.html?source=search" title="iOS" target="_blank">iOS</a>
            &nbsp;
            <span class="c9">[杭州]</span>
        </div>
        <span><em class="c7">月薪：</em>6k-10k</span>
        <span><em class="c7">经验：</em> 不限</span>
        <span><em class="c7">最低学历： </em>不限</span>
        <br/>
        <span><em class="c7">职位诱惑：</em>一年两次调薪，团队旅游，丰厚年终无限零食</span>
        <br/>
        <span>13:22发布</span>
    </div>
    <div class="hot_pos_r">
        <div class="apply">
            <a href="http://www.lagou.com/jobs/103591.html?source=search" target="_blank">投个简历</a>
        </div>
        <div class="mb10"><a href="http://www.lagou.com/gongsi/18261.html" title="中焯信息" target="_blank">中焯信息</a></div>
        <span><em class="c7">领域： </em>金融互联网</span>
        <span><em class="c7">创始人：</em> 周林根</span>
        <br/>
        <span><em class="c7">阶段： </em>成长型(A轮)</span>
        <span><em class="c7">规模： </em>50-150人</span>
        <ul class="companyTags reset">
            <li>五险一金</li>
            <li>通讯津贴</li>
            <li>交通补助</li>
        </ul>
    </div>
</li>

<li class="clearfix">
    <div class="hot_pos_l">
        <div class="mb10">
            <a href="http://www.lagou.com/jobs/156813.html?source=search" title="iOS" target="_blank">iOS</a>
            &nbsp;
            <span class="c9">[南京]</span>
        </div>
        <span><em class="c7">月薪：</em>6k以上</span>
        <span><em class="c7">经验：</em> 1-3年</span>
        <span><em class="c7">最低学历： </em>不限</span>
        <br/>
        <span><em class="c7">职位诱惑：</em>专业的移动金融平台，优越的晋升空间</span>
        <br/>
        <span>09:05发布</span>
    </div>
    <div class="hot_pos_r">
        <div class="apply">
            <a href="http://www.lagou.com/jobs/156813.html?source=search" target="_blank">投个简历</a>
        </div>
        <div class="mb10"><a href="http://www.lagou.com/gongsi/22025.html" title="屹通信息" target="_blank">屹通信息</a></div>
        <span><em class="c7">领域： </em>移动互联网</span>
        <br/>
        <span><em class="c7">阶段： </em>上市公司</span>
        <span><em class="c7">规模： </em>150-500人</span>
        <ul class="companyTags reset">
            <li>绩效奖金</li>
            <li>股票期权</li>
            <li>年底双薪</li>
        </ul>
    </div>
</li>

<li class="odd clearfix">
    <div class="hot_pos_l">
        <div class="mb10">
            <a href="http://www.lagou.com/jobs/109067.html?source=search" title="iOS" target="_blank">iOS</a>
            &nbsp;
            <span class="c9">[北京]</span>
        </div>
        <span><em class="c7">月薪：</em>8k-16k</span>
        <span><em class="c7">经验：</em> 3-5年</span>
        <span><em class="c7">最低学历： </em>本科</span>
        <br/>
        <span><em class="c7">职位诱惑：</em>六险一金+诱人年终奖+各种福利</span>
        <br/>
        <span>10:55发布</span>
    </div>
    <div class="hot_pos_r">
        <div class="apply">
            <a href="http://www.lagou.com/jobs/109067.html?source=search" target="_blank">投个简历</a>
        </div>
        <div class="mb10"><a href="http://www.lagou.com/gongsi/7626.html" title="网利宝" target="_blank">网利宝</a></div>
        <span><em class="c7">领域： </em>O2O,金融互联网</span>
        <span><em class="c7">创始人：</em> 赵润龙（Kevin Chiu）</span>
        <br/>
        <span><em class="c7">阶段： </em>成长型(A轮)</span>
        <span><em class="c7">规模： </em>15-50人</span>
        <ul class="companyTags reset">
            <li>股票期权</li>
            <li>五险一金</li>
            <li>带薪年假</li>
        </ul>
    </div>
</li>

<li class="clearfix">
    <div class="hot_pos_l">
        <div class="mb10">
            <a href="http://www.lagou.com/jobs/119369.html?source=search" title="iOS" target="_blank">iOS</a>
            &nbsp;
            <span class="c9">[上海]</span>
        </div>
        <span><em class="c7">月薪：</em>7k-11k</span>
        <span><em class="c7">经验：</em> 1-3年</span>
        <span><em class="c7">最低学历： </em>大专</span>
        <br/>
        <span><em class="c7">职位诱惑：</em>房补，饭补， 带薪年假， 弹性工作，体检</span>
        <br/>
        <span>3天前发布</span>
    </div>
    <div class="hot_pos_r">
        <div class="apply">
            <a href="http://www.lagou.com/jobs/119369.html?source=search" target="_blank">投个简历</a>
        </div>
        <div class="mb10"><a href="http://www.lagou.com/gongsi/14229.html" title="微盟" target="_blank">微盟</a></div>
        <span><em class="c7">领域： </em>移动互联网,O2O</span>
        <span><em class="c7">创始人：</em> 孙涛勇</span>
        <br/>
        <span><em class="c7">阶段： </em>成长型(A轮)</span>
        <span><em class="c7">规模： </em>150-500人</span>
        <ul class="companyTags reset">
            <li>绩效奖金</li>
            <li>年底双薪</li>
            <li>五险一金</li>
        </ul>
    </div>
</li>

<li class="odd clearfix">
    <div class="hot_pos_l">
        <div class="mb10">
            <a href="http://www.lagou.com/jobs/171011.html?source=search" title="iOS" target="_blank">iOS</a>
            &nbsp;
            <span class="c9">[北京]</span>
        </div>
        <span><em class="c7">月薪：</em>13k-25k</span>
        <span><em class="c7">经验：</em> 1-3年</span>
        <span><em class="c7">最低学历： </em>本科</span>
        <br/>
        <span><em class="c7">职位诱惑：</em>福利待遇好，弹性上下班，工作环境优雅</span>
        <br/>
        <span>11:17发布</span>
    </div>
    <div class="hot_pos_r">
        <div class="apply">
            <a href="http://www.lagou.com/jobs/171011.html?source=search" target="_blank">投个简历</a>
        </div>
        <div class="mb10"><a href="http://www.lagou.com/gongsi/1970.html" title="去哪儿网" target="_blank">去哪儿网</a></div>
        <span><em class="c7">领域： </em>移动互联网</span>
        <span><em class="c7">创始人：</em> 庄辰超</span>
        <br/>
        <span><em class="c7">阶段： </em>上市公司</span>
        <span><em class="c7">规模： </em>2000人以上</span>
        <ul class="companyTags reset">
            <li>技能培训</li>
            <li>节日礼物</li>
            <li>带薪年假</li>
        </ul>
    </div>
</li>

</ul>
<div class="Pagination"></div>
</div>

<script>
    $(function () {
        /***************************
         * 分页
         */
        $('.Pagination').pager({
            currPage: 1,
            pageNOName: "pn",
            form: "searchForm",
            pageCount: 30,
            pageSize: 5
        });

        $(".workplace dd").not('.more').children('a').click(function () {
            var cityVal = $(this).text();
            var cityjob = $('#box_expectCity').attr("data-cityjob");
            if (cityjob == 'true') {
                return true;
            } else {
                $('#lc').val(1);
                editForm("cityInput", cityVal);
                return false;
            }
        });

        $("#box_expectCity dd a").click(function () {
            var cityjob = $('#box_expectCity').attr("data-cityjob");
            var cityVal = $(this).text();
            if (cityjob == 'true') {
                return true;
            } else {
                $('#lc').val(1);
                editForm("cityInput", cityVal);
                return false;
            }
        });

        $('#options dd div').click(function () {
            var firstName = $(this).parents('dl').children('dt').text();
            var fn = $.trim(firstName);
            if (fn == "月薪范围") {
                editForm("yxInput", $(this).html());
            }
            else if (fn == "工作经验") {
                editForm("gjInput", $(this).html());
            }
            else if (fn == "最低学历") {
                editForm("xlInput", $(this).html());
            }
            else if (fn == "工作性质") {
                editForm("gxInput", $(this).html());
            }
            else if (fn == "发布时间") {
                editForm("stInput", $(this).html());
            }
        });

        $('#selected ul').delegate('li span.select_remove', 'click', function (event) {
            var firstName = $(this).parent('li').find('strong').text();
            var fn = $.trim(firstName);
            if (fn == "月薪范围")
                editForm("yxInput", "");
            else if (fn == "工作经验")
                editForm("gjInput", "");
            else if (fn == "最低学历")
                editForm("xlInput", "");
            else if (fn == "工作性质")
                editForm("gxInput", "");
            else if (fn == "发布时间")
                editForm("stInput", "");
        });

        /* search结果飘绿	*/
        (function ($) {
            var searchVal = $('#search_input').val();
            var reg = /\s/g;
            searchVal = searchVal.replace(reg, "").split("");

            var resultL = '';
            var resultR = '';
            $('.hot_pos li').each(function () {
                resultL = $('.hot_pos_l a', this).text().split("");
                $.each(resultL, function (i, v) {
                    if ($.inArray(v.toLowerCase(), searchVal) != -1 || $.inArray(v.toUpperCase(), searchVal) != -1) {
                        resultL[i] = '<span>' + v + '</span>';
                    }
                });
                $('.hot_pos_l a', this).html(resultL.join(''));

                resultR = $('.hot_pos_r .mb10 a', this).text().split("");
                $.each(resultR, function (i, v) {
                    if ($.inArray(v.toLowerCase(), searchVal) != -1 || $.inArray(v.toUpperCase(), searchVal) != -1) {
                        resultR[i] = '<span>' + v + '</span>';
                    }
                });
                $('.hot_pos_r .mb10 a', this).html(resultR.join(''));
            });

        })(jQuery);

        //didi tip
        if ($.cookie('didiTip') != 1 && false) {
            $('#tip_didi').show();
        }
        $('#tip_didi a').click(function () {
            $(this).parent().remove();
            $.cookie('didiTip', 1, { expires: 30, path: '/'});
        });

        //统计到职位详情的来源脚本 add by Vee ｜2014-7-3
        /* $('.hot_pos .hot_pos_l a').click(function(e){
         e.preventDefault();
         var href=$(this).attr("href");
         window.location.href=href+"?source=search"; //搜索结果页
         }); */
        //end
        $(".partime_delete").click(function () {
            $(this).parent().hide();
            $(".partime_weixin").show();
        })
    });

    function editForm(inputId, inputValue) {
        $("#" + inputId).val(inputValue);
        var keyword = $.trim($('#search_input').val());
        var reg = /[`~!@\$%\^\&\*\(\)_<>\?:"\{\},\\\/;'\[\]]/ig;
        var re = /#/g;
        var r = /\./g;
        var kw = keyword.replace(reg, " ");
        if (kw == '') {
            $('#searchForm').attr('action', 'http://www.lagou.com/jobs/list_所有职位').submit();
        } else {
            kw = kw.replace(re, '井');
            kw = kw.replace(r, '。');
            $('#searchForm').attr('action', 'http://www.lagou.com/jobs/list_' + kw).submit();
        }
        //$("#searchForm").submit();
    }
</script>

<div class="clear"></div>
<input type="hidden" id="resubmitToken" value=""/>
<a id="backtop" title="回到顶部" rel="nofollow"></a>
</div>
<!-- end #container -->
</div>
<!-- end #body -->
<div id="footer">
    <div class="wrapper">
        <a href="http://www.lagou.com/about.html" target="_blank" rel="nofollow">联系我们</a>
        <a href="http://www.lagou.com/af/zhaopin.html" target="_blank">互联网公司导航</a>
        <a href="http://e.weibo.com/lagou720" target="_blank" rel="nofollow">拉勾微博</a>
        <a class="footer_qr" href="javascript:void(0)" rel="nofollow">拉勾微信<i></i></a>

        <div class="copyright">&copy;2013-2014 Lagou <a target="_blank"
                                                        href="http://www.miitbeian.gov.cn/state/outPortal/loginPortal.action">京ICP备14023790号-2</a>
        </div>
    </div>
</div>

<script type="text/javascript" src="http://www.lagou.com/js/core.min.js?v=1.5.5.5_0722_14"></script>
<script type="text/javascript" src="http://www.lagou.com/js/popup.min.js?v=1.5.5.5_0722_14"></script>
<script type="text/javascript" src="http://www.lagou.com/js/libs/tongji.js?v=1.5.5.5_0722_14"></script>
<script type="text/javascript" src="http://www.lagou.com/js/libs/analytics.js?v=1.5.5.5_0722_14"></script>
<script type="text/javascript">
    $(function () {
        $('#noticeDot-1').hide();
        $('#noticeTip a.closeNT').click(function () {
            $(this).parent().hide();
        });
    });
    var index = Math.floor(Math.random() * 2);
    var ipArray = new Array('42.62.79.226', '42.62.79.227');
    var url = "ws://" + ipArray[index] + ":18080/wsServlet?code=434773";
    var CallCenter = {
        init: function (url) {
            var _websocket = new WebSocket(url);
            _websocket.onopen = function (evt) {
                console.log("Connected to WebSocket server.");
            };
            _websocket.onclose = function (evt) {
                console.log("Disconnected");
            };
            _websocket.onmessage = function (evt) {
                //alert(evt.data);
                var notice = jQuery.parseJSON(evt.data);
                if (notice.status[0] == 0) {
                    $('#noticeDot-0').hide();
                    $('#noticeTip').hide();
                    $('#noticeNo').text('').show().parent('a').attr('href', ctx + '/mycenter/delivery.html');
                    $('#noticeNoPage').text('').show().parent('a').attr('href', ctx + '/mycenter/delivery.html');
                } else {
                    $('#noticeDot-0').show();
                    $('#noticeTip strong').text(notice.status[0]);
                    $('#noticeTip').show();
                    $('#noticeNo').text('(' + notice.status[0] + ')').show().parent('a').attr('href', ctx + '/mycenter/delivery.html?tag=0&r=0');
                    $('#noticeNoPage').text(' (' + notice.status[0] + ')').show().parent('a').attr('href', ctx + '/mycenter/delivery.html?tag=0&r=0');
                }
                $('#noticeDot-1').hide();
            };
            _websocket.onerror = function (evt) {
                console.log('Error occured: ' + evt);
            };
        }
    };
    CallCenter.init(url);
</script>
</body>
</html>'''

# a = Analiser(html)
# mark = {'name': 'ul', 'attrs': {'class': 'hot_pos reset'}}
# l = a.Analy(mark)
# print l
# s=str(l)
# print type(s)
# print s

html_doc = '''
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=10;IE=9;IE=8;IE=7;IE=EDGE">
<!-- <script id="allmobilize" charset="utf-8" src="${rbase}/js/third/allmobilize.min.js"></script> -->
<script id="allmobilize" charset="utf-8" src="http://a.yunshipei.com/ef48a3377914af6ef846830fcae2a8e6/allmobilize.min.js"></script>
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="alternate" media="handheld" href="#" />
<!-- end 云适配 -->
<meta charset="utf-8">

<title>Python招聘-果壳网招聘-拉勾网</title>
<meta property="qc:admins" content="23635710066417756375" />
<meta content="Python 北京 本科 1-3年 全职 后端开发 弹性工作时间 家庭式午餐 自由的工作环境 果壳网 北京果壳互动信息技术有限公司 提供泛科学内容服务和社区讨论，以及MOOC领域最大的中文社区 拉勾网-最专业的互联网招聘平台" name="description">
<meta content="Python 北京 本科 1-3年 全职 后端开发 弹性工作时间 家庭式午餐 自由的工作环境 果壳网 北京果壳互动信息技术有限公司 提供泛科学内容服务和社区讨论，以及MOOC领域最大的中文社区 拉勾网-最专业的互联网招聘平台" name="keywords">
<meta name="baidu-site-verification" content="QIQ6KC1oZ6" />

<!-- <div class="web_root"  style="display:none">http://www.lagou.com</div> -->
<script type="text/javascript">
var ctx = "http://www.lagou.com";
var rctx = "http://hr.lagou.com";
</script><link rel="Shortcut Icon" href="http://www.lagou.com/images/favicon.ico">
<link rel="stylesheet" type="text/css" href="http://www.lagou.com/css/style.css?v=1.5.5.6_0825"/>
<link rel="stylesheet" type="text/css" href="http://hr.lagou.com/css/style.css?v=1.5.5.6_0825"/>
<link rel="stylesheet" type="text/css" href="http://www.lagou.com/css/external.min.css?v=1.5.5.6_0825"/>
<link rel="stylesheet" type="text/css" href="http://www.lagou.com/css/popup.css?v=1.5.5.6_0825"/>
<script src="http://www.lagou.com/js/libs/jquery.1.10.1.min.js?v=1.5.5.6_0825" type="text/javascript"></script>
<script type="text/javascript" src="http://www.lagou.com/js/libs/jquery.lib.min.js?v=1.5.5.6_0825"></script>
<script src="http://www.lagou.com/js/ajaxfileupload.js?v=1.5.5.6_0825" type="text/javascript"></script>
<script src="http://www.lagou.com/js/libs/jquery-hbzx.js?v=1.5.5.6_0825" type="text/javascript"></script>
<!--[if lte IE 8]>
    <script type="text/javascript" src="http://www.lagou.com/js/assets/excanvas.js?v=1.5.5.6_0825"></script>
<![endif]-->
<script type="text/javascript">
var youdao_conv_id = 271546;
</script>
<script type="text/javascript" src="http://conv.youdao.com/pub/conv.js"></script>
</head>
<body>
<div id="body">
	<div id="header">
    	<div class="wrapper">
    		<a href="http://www.lagou.com/" class="logo"></a>
    		<ul class="reset" id="navheader">
    			<li ><a href="http://www.lagou.com/">首页</a></li>
    			<li ><a href="http://www.lagou.com/gongsi/" >公司</a></li>
    			<li ><a href="http://www.lagou.com/toForum.html" target="_blank" rel="nofollow">论坛</a></li>
    				    			<li ><a href="http://www.lagou.com/resume/basic.html" rel="nofollow">我的简历</a></li>
	    						    		</ul>
        	        	<dl class="collapsible_menu">
            	<dt>
           			<span id="nowrap">刘啸驹&nbsp;</span>
           								<span id="noticeDot-0"  class="red" ></span>
            		            		<i></i>
            	</dt>
                                	<dd><a href="http://www.lagou.com/resume/myresume.html" rel="nofollow">我的简历</a></dd>
                	                	<dd><a href="http://www.lagou.com/mycenter/collections.html">我收藏的职位</a></dd>
                	                	                	<dd><a  href="http://www.lagou.com/mycenter/delivery.html?tag=0&r=0" >我投递的职位 <span  class="red"  id="noticeNo">(1)</span></a></dd>
                	                	<dd class="btm"><a href="http://www.lagou.com/s/subscribe.html">我的订阅</a></dd>
                	<dd><a href="http://www.lagou.com/mycenter/saveUserType.do?type=1">我要招人</a></dd>
                                                <dd><a href="http://www.lagou.com/user/accountBind.html">帐号设置</a></dd>
                                <dd class="logout"><a href="http://www.lagou.com/user/logout.html" rel="nofollow">退出</a></dd>
            </dl>
                                    <div id="noticeTip" >
            	<span class="bot"></span>
				<span class="top"></span>
				<a href="http://www.lagou.com/mycenter/delivery.html?tag=0&r=0"  class="notice_tip" target="_blank"><strong>1</strong>条新投递反馈</a>
				<a href="javascript:;" class="closeNT"></a>
            </div>
                    </div>
    </div><!-- end #header -->
    <div id="container">
                <div class="clearfix">
            <div class="content_l">
            	                <dl class="job_detail">
                    <dt>
                        <h1 title="Python">
                            <em></em>
                                                        	<div>果壳网招聘</div>
                           	                           	Python
                        </h1>


                       	                       	<div id="jobCollection"  class="jd_collection" >
                       		                       		<div class="jd_collection_success">
                       			<span>已成功收藏该职位，</span>
								<a href="http://www.lagou.com/mycenter/collections.html" class="jd_collection_page">查看全部</a>
								<a href="javascript:;" class="jd_collection_x"></a>
							</div>
                       	</div>
                       	                    </dt>
                    <dd class="job_request">
                    	<span class="red">10k-15k</span>
                       	<span>北京</span>
                       	<span>经验1-3年 </span>
                       	<span> 本科及以上</span>
                       	<span>全职</span><br />
                      	  职位诱惑 : 弹性工作时间 家庭式午餐 自由的工作环境
                      	<div>发布时间：1天前发布</div>
                    </dd>
                    <dd class="job_bt">
                        <h3 class="description">职位描述</h3>
                        <p>职位描述：</p>
<p>根据产品需求设计、</p>
<p>开发及维护果壳网相关系统、功能；</p>
<p>主要使用Python语言进行开发；</p>
<p>接受实习生；&nbsp;</p>
<p>职位要求：</p>
<p>本科以上学历，熟练掌握编程基础知识，热爱编程；</p>
<p>掌握Python语言，了解Flask等Web框架；</p>
<p>了解WEB开发，理解相关概念，有实际项目经验；</p>
<p>具备良好的沟通能力及团队合作精神；&nbsp;</p>
<p>优先条件：简历写明技术博客地址；简历写明Github帐号</p>
                    </dd>

                                        	<!-- 用户是否激活 0-否；1-是 -->
	                			       									                    <dd class="resume">
				                        <div style="width:400px;">
								          <span> 你已设置默认投递简历：<a href="http://www.lagou.com/nearBy/downloadResume" target="_blank" title="下载Python开发工程师简历.pdf"><strong>附件简历：Python开发工程...</strong></a></span><br  />
								          <span>设置于2014-08-13 23:39</span>
				                        </div>
				                        <a class="inline fl" href="#setResume" title="设置默认投递简历">重新设置</a>
				                    </dd>
				                 			            	                                                        <div class="saoma saoma_btm">
                      	<div class="dropdown_menu">
							<div class="drop_l">
								<img src="http://www.lagou.com/images/qr/job_qr_btm.png" width="131" height="131" />
																<span>[仅限本人使用]</span>
															</div>
							<div class="drop_r">
								<div class="drop_title"></div>
								<p>
									想知道HR在看简历嘛？<br />
          							想在微信中收到面试通知？<br />
          							<span><< 扫一扫，给你解决</span>
								</p>
							</div>
						</div>
                    </div>
                                        <dd>
                                        	                    				                   		<!-- 用户是否激活 0-否；1-是 -->

	                   					                        									                 	<a href="javascript:sendResume(434773,146485,false,false);" class="btn fr btn_apply" title="投个简历">投个简历</a>

		                        	                        	                   		                	                </dd>
                </dl>
                                <div id="weibolist"></div>
            </div>
            <div class="content_r">
                <dl class="job_company">
                    <dt>
                    	<a href="http://www.lagou.com/gongsi/11209.html" target="_blank">
                            <img class="b2" src="http://www.lagou.com/upload/logo/ff8080814503eb8f014517837f2377a4.png" width="80" height="80" alt="北京果壳互动信息技术有限公司" />
                            <div>
                                <h2 class="fl">
                                	                                  		果壳网

                                  	                                    	<img src="http://www.lagou.com/images/valid.png" width="15" height="19" alt="拉勾认证企业" />
                                    	<span class="dn">拉勾认证企业</span>

                                </h2>
                            </div>
                        </a>
                    </dt>
                    <dd>
                    	<ul class="c_feature reset">
                        	<li><span>领域</span> 移动互联网,媒体</li>
                        	<li><span>规模</span> 150-500人</li>
                        	<li>
                        		<span>主页</span>
                        		           							<a href="http://www.guokr.com" target="_blank" title="http://www.guokr.com" rel="nofollow">http://www.guokr.com</a>
           						                        	</li>
                        </ul>

                        <h4>发展阶段</h4>
                        <ul class="c_feature reset">
                        	<li><span>目前阶段</span> B轮</li>
                        	                        </ul>

                        <!--	                    	<h4>公司产品</h4>
	                        <ul class="c_feature reset">
	                        		                        		<li><span>研究生</span></li>
	                        		                        		<li><span>知性</span></li>
	                        		                        		<li><span>MOOC学院</span></li>
	                        		                        </ul>

                       	<h4>公司标签</h4>
                        <ul class="company_tags reset" id="hasLabels">
                        	                            	<li><span>绩效奖金</span></li>
                                                        	<li><span>股票期权</span></li>
                                                        	<li><span>五险一金</span></li>
                                                        	<li><span>带薪年假</span></li>
                                                        	<li><span>定期体检</span></li>
                                                        	<li><span>岗位晋升</span></li>
                                                        	<li><span>领导好</span></li>
                                                        	<li><span>家庭口味午餐</span></li>
                                                        	<li><span>每日水果</span></li>
                                                        	<li><span>很科学</span></li>
                                                        	<li><span>Geek</span></li>
                                                        	<li><span>羽毛球俱乐部</span></li>
                                                        	<li><span>电影协会</span></li>
                                                        	<li><span>桌游俱乐部</span></li>
                                                        	<li><span>毽协</span></li>
                                                        	<li><span>节日礼物</span></li>
                                                        	<li><span>年度旅游</span></li>
                                                        	<li><span>技能培训</span></li>
                                                       <li class="link"><a>编辑</a></li>
                        </ul>
                        <div class="clear"></div>
                        <div id="addLabel" class="addLabel dn">
                            <input type="text" class="fl" id="label" name="label" placeholder="添加自定义标签" />
                            <input type="submit" id="add" value="贴上" />
                        </div> -->

                       	                       	<h4>工作地址</h4>
                       	<div>北京市朝阳区郎家园六号院3号楼4层</div>
                       	<div id="smallmap"></div>
                       	<a href="javascript:;" id="mapPreview">查看完整地图</a>
                       	                    </dd>
                </dl>
                 <!--  -->
                 <!-- write by Darren  -->
                <div id="position_recommend">
                   <ul class="position_head">
                                            <li class="guess_selected">相似职位</li>
                      <li>猜你喜欢</li>
                                         </ul>
                   <div class="similar_content">
                      <div class="position_detail_content">
                                                  <dl>
		                     <dt><a href="http://www.lagou.com/gongsi/12557.html?source=position_rec" title="百分点" target="_blank"><img src="http://www.lagou.com/upload/logo/ff80808145413f0c0145447b3470778c.jpg" alt="百分点" width="54" height="54" /></a></dt>
		                     <dd class="content_inner">
		                        <a href="http://www.lagou.com/jobs/183483.html?source=position_rec" target="_blank">
			                        <h2 title="Python">Python</h2>
			                        <span>10k-15k</span>
			                        			                        <p title="百分点">百分点</p>
			                        		                        </a>
                             </dd>
                         </dl>
                                                  <dl>
		                     <dt><a href="http://www.lagou.com/gongsi/17001.html?source=position_rec" title="信柏" target="_blank"><img src="http://www.lagou.com/upload/logo/ff80808145ae04790145da47aa5f445e.jpg" alt="信柏" width="54" height="54" /></a></dt>
		                     <dd class="content_inner">
		                        <a href="http://www.lagou.com/jobs/134136.html?source=position_rec" target="_blank">
			                        <h2 title="Python">Python</h2>
			                        <span>10k-15k</span>
			                        			                        <p title="信柏">信柏</p>
			                        		                        </a>
                             </dd>
                         </dl>
                                                  <dl>
		                     <dt><a href="http://www.lagou.com/gongsi/18321.html?source=position_rec" title="火花点点" target="_blank"><img src="http://www.lagou.com/upload/logo/ff808081458a813401458d6ec710184b.jpg" alt="火花点点" width="54" height="54" /></a></dt>
		                     <dd class="content_inner">
		                        <a href="http://www.lagou.com/jobs/146211.html?source=position_rec" target="_blank">
			                        <h2 title="Python">Python</h2>
			                        <span>10k-15k</span>
			                        			                        <p title="火花点点">火花点点</p>
			                        		                        </a>
                             </dd>
                         </dl>
                                                  <dl>
		                     <dt><a href="http://www.lagou.com/gongsi/413.html?source=position_rec" title="布丁移动" target="_blank"><img src="http://www.lagou.com/upload/logo/fa6247a24111445f0141163a370e00ca.png" alt="布丁移动" width="54" height="54" /></a></dt>
		                     <dd class="content_inner">
		                        <a href="http://www.lagou.com/jobs/9464.html?source=position_rec" target="_blank">
			                        <h2 title="Python开发工程师">Python开发工程师</h2>
			                        <span>10k-15k</span>
			                        			                        <p title="布丁移动">布丁移动</p>
			                        		                        </a>
                             </dd>
                         </dl>
                                                  <dl>
		                     <dt><a href="http://www.lagou.com/gongsi/19083.html?source=position_rec" title="博派通达" target="_blank"><img src="http://www.lagou.com/upload/logo/a42dd003320744f2a4780d4e3ac6d138.png" alt="博派通达" width="54" height="54" /></a></dt>
		                     <dd class="content_inner">
		                        <a href="http://www.lagou.com/jobs/130363.html?source=position_rec" target="_blank">
			                        <h2 title="Python开发工程师">Python开发工程师</h2>
			                        <span>10k-15k</span>
			                        			                        <p title="博派通达">博派通达</p>
			                        		                        </a>
                             </dd>
                         </dl>
                                                  <a href="http://www.lagou.com/jobs/list_Python?kd=Python&spc=1&pl=&gj=1-3年&xl=&yx=&gx=&st=&labelWords=&lc=&workAddress=&city=北京" class="similar_position_footer" target="_blank" id="similar_footer">查看更多相似职位</a>
                       </div>
                   </div>
                                      <div id="guess">
	                   <div class="guess_like">
	                   		                   <p>暂时没有匹配的职位</p>
		                   <p class="special_fix">不妨调整你的期望工作试一试</p>
		                   <p><a href="http://www.lagou.com/resume/myresume.html#expectJob" title="前去修改" target="_blank" class="guess_rectangle">前去修改</a></p>
		                   	                   </div>
                   </div>
                                   </div>
               <!--  End -->
				<!-- 广告位：主站--职位详情页 -->
                <script type="text/javascript" >BAIDU_CLB_SLOT_ID = "946156";</script>
                <script type="text/javascript" src="http://cbjs.baidu.com/js/o.js"></script>
            </div><!--end #myRecommend-->
      </div>
      <input type="hidden" value="4e8238bd34c5a7a460b7dece39a97c3e" name="userid" id="userid" />
      <input type="hidden" value="0" name="type" id="resend_type" />
      <input type="hidden" value="146485"  id="jobid" />
      <input type="hidden" value="11209"  id="companyid" />
      <input type="hidden" value="" id="positionLng" />
      <input type="hidden" value="" id="positionLat" />

		<div id="tipOverlay" ></div>
<!-------------------------------------弹窗lightbox  ----------------------------------------->
<div style="display:none;">
	<!-- 设置默认投递简历 -->
	<div id="setResume" class="popup" style="height:280px;">
	    <table width="100%">
	    	<tr>
	    		<td class="f18 c5">请选择你要投出去的简历：</td>
	    	</tr>
	    	<tr>
	        	<td>
                    <form  id="resumeSetForm" class="resumeSetForm">
	            		<label class="radio">
	            			<input type="radio" name="resumeName" class="resume1" value="1"  />
	            			在线简历：
	            				            				<span title="刘啸驹的简历">刘啸驹的简历</span>
	            				            		</label>
            			<div class="setBtns">
            					            				<a href="http://www.lagou.com/resume/preview.html" target="_blank">预览</a> |
	            										<a href="http://www.lagou.com/resume/myresume.html" target="_blank">修改</a>
            			</div>
	            		<div class="clear"></div>
	            		<label class="radio">
	            			<input type="radio" name="resumeName" class="resume0" value="0"  checked  />
	            			附件简历：
	            				            				<span class="uploadedResume" title="Python开发工程师简历.pdf">Python开发工程师简历.pdf</span>
	            				            		</label>
	            		<div class="setBtns">
	            				            				<a href="http://www.lagou.com/nearBy/downloadResume" class="downloadResume">下载</a> <span>|</span>
            					<a target="_blank" title="上传附件简历" class="reUpload">重新上传</a>

            				<input title="支持word、pdf、ppt、txt、wps格式文件，大小不超过10M"  name="newResume" id="reUploadResume1"
	                         type="file" onchange="file_check(this,'http://www.lagou.com/nearBy/updateMyResume.json','reUploadResume1')" />
            			</div>
            			<div class="clear"></div>
            			<span class="error" style="display:none;">只支持word、pdf、ppt、txt、wps格式文件，请重新上传</span>
	            		<label class="bgPink">默认使用此简历直接投递，下次不再提示</label>
	            		<span class="setTip error"></span>
	            		<input type="submit" class="btn_profile_save btn_s" value="保存设置" />
	            	</form>
	            </td>
	        </tr>
	    </table>
	</div><!--/#setResume-->

<!-- 投递简历时  - 设置默认投递简历 -->
	<div id="setResumeApply" class="popup" style="height:280px;">
	    <table width="100%">
	    	<tr>
	    		<td class="f18 c5">请选择你要投出去的简历：</td>
	    	</tr>
	    	<tr>
	        	<td>
                    <form  id="resumeSendForm" class="resumeSetForm">
	            		<label class="radio">
	            			<input type="radio" name="resumeName" class="resume1" value="1"  />
	            			在线简历：
	            				            				<span title="刘啸驹的简历">刘啸驹的简历</span>
	            				            		</label>
            			<div class="setBtns">
            					            				<a href="http://www.lagou.com/resume/preview.html" target="_blank">预览</a> |
	            										<a href="http://www.lagou.com/resume/myresume.html" target="_blank">修改</a>
            			</div>
	            		<div class="clear"></div>
	            		<label class="radio">
	            			<input type="radio" name="resumeName" class="resume0" value="0"  checked  />
	            			附件简历：
	            				            				<span class="uploadedResume" title="Python开发工程师简历.pdf">Python开发工程师简历.pdf</span>
	            				            		</label>
	            		<div class="setBtns">
	            				            				<a href="http://www.lagou.com/nearBy/downloadResume" class="downloadResume">下载</a> <span>|</span>
            					<a target="_blank" title="上传附件简历" class="reUpload">重新上传</a>
	            				            			<input title="支持word、pdf、ppt、txt、wps格式文件，大小不超过10M"  name="newResume" id="reUploadResume2"
	                        type="file" onchange="file_check(this,'http://www.lagou.com/nearBy/updateMyResume.json','reUploadResume2')" />
            			</div>
            			<div class="clear"></div>
            			<span class="error" style="display:none;">只支持word、pdf、ppt、txt、wps格式文件，请重新上传</span>
	            		<label class="bgPink"><input type="checkbox" checked="checked" />默认使用此简历直接投递，下次不再提示</label>
	            		<span class="setTip error"></span>
	            		<input type="submit" class="btn_profile_save btn_s" value="确认投递简历" />
	            	</form>
	            </td>
	        </tr>
	    </table>
	</div><!--/#setResumeApply-->

	<!-- 上传简历 -->
	<div id="uploadFile" class="popup">
	    <table width="100%">
	    	<tr>
	        	<td align="center">
	                <form>
	                    <a href="javascript:void(0);" class="btn_addPic">
	                    	<span>选择上传文件</span>
	                        <input tabindex="3" title="支持word、pdf、ppt、txt、wps格式文件，大小不超过10M" size="3" name="newResume" id="resumeUpload"
	                        class="filePrew" type="file" onchange="file_check(this,'http://www.lagou.com/nearBy/updateMyResume.json','resumeUpload')" />
	                    </a>
	                </form>
	            </td>
	        </tr>
	    	<tr>
	        	<td align="left">支持word、pdf、ppt、txt、wps格式文件<br />文件大小需小于10M</td>
	        </tr>
	        <tr>
	        	<td align="left" style="color:#dd4a38; padding-top:10px;">注：若从其它网站下载的word简历，请将文件另存为.docx格式后上传</td>
	        </tr>
	    	<tr>
	        	<td align="center"><img src="http://www.lagou.com/images/loading.gif" width="55" height="16" id="loadingImg" style="visibility: hidden;" alt="loading" /></td>
	        </tr>
	    </table>
	</div><!--/#uploadFile-->

	<!-- 简历上传成功 -->
	<div id="uploadFileSuccess" class="popup">
     	<h4>简历上传成功！</h4>
         <table width="100%">
             <tr>
                 <td align="center"><p>你可以将简历投给你中意的公司了。</p></td>
             </tr>
         	<tr>
             	<td align="center"><a href="javascript:top.location.reload();" class="btn_s">确&nbsp;定</a></td>
             </tr>
         </table>
     </div><!--/#uploadFileSuccess-->

	<!-- 登录框 -->
	<div id="loginPop" class="popup" style="height:240px;">
       	<form id="loginForm">
			<input type="text" id="email" name="email" tabindex="1" placeholder="请输入登录邮箱地址" />
			<input type="password" id="password" name="password" tabindex="2" placeholder="请输入密码" />
			<span class="error" style="display:none;" id="beError"></span>
		    <label class="fl" for="remember"><input type="checkbox" id="remember" value="" checked="checked" name="autoLogin" /> 记住我</label>
		    <a href="http://www.lagou.com/reset.html" class="fr">忘记密码？</a>
		    <input type="submit" id="submitLogin" value="登 &nbsp; &nbsp; 录" />
		</form>
		<div class="login_right">
			<div>还没有拉勾帐号？</div>
			<a href="http://www.lagou.com/register.html" class="registor_now">立即注册</a>
		    <div class="login_others">使用以下帐号直接登录:</div>
		    <a href="http://www.lagou.com/ologin/auth/sina.html" target="_blank" class="icon_wb" title="使用新浪微博帐号登录"></a>
		    <a href="http://www.lagou.com/ologin/auth/qq.html" class="icon_qq" target="_blank" title="使用腾讯QQ帐号登录" ></a>
		</div>
    </div><!--/#loginPop-->

    <!-- 投简历成功前填写基本信息 -->
    <style>
    #cboxContent{overflow:visible;}
    #colorbox, #cboxOverlay, #cboxWrapper{overflow:visible;}
    </style>
    <div id="infoBeforeDeliverResume" class="popup" style="height:300px; overflow:visible;">
    	<div class="f18">为方便所投递企业HR查阅，请确认个人信息</div>
    	<form id="basicInfoForm" method="post">
	        <table width="100%">
	            <tr>
	                <td valign="middle">
				        <span class="redstar">*</span>
				    </td>
				    <td>
				        <input type="text" name="name" placeholder="姓名" />
				    </td>
				    <td valign="middle">
				        <span class="redstar">*</span>
				    </td>
				    <td>
				        <input type="hidden" name="degree" value="" id="degree" />
				        <input type="button" class="profile_select_190 profile_select_normal" id="select_degree"  value="最高学历"  />
						<div id="box_degree" class="boxUpDown boxUpDown_190 dn"></div>
				    </td>
	            </tr>
	            <tr>
	                <td valign="middle">
				        <span class="redstar">*</span>
				    </td>
				    <td>
				        <input type="hidden" name="workyear" value="" id="workyear" />
				        <input type="button" class="profile_select_190 profile_select_normal" id="select_workyear"  value="工作年限" />
						<div id="box_workyear" class="boxUpDown boxUpDown_190 dn"></div>
				    </td>
				    <td valign="middle">
				        <span class="redstar">*</span>
				    </td>
				    <td>
				        <input type="hidden" name="expectCity" value="" id="expectCity" />
				        <input type="button" class="profile_select_190 profile_select_normal" id="select_expectCity" value="期望工作城市"  />
						<div id="box_expectCity" class="boxUpDown boxUpDown_596 dn"></div>
				    </td>
	            </tr>
	            <tr>
	                <td valign="middle">
				        <span class="redstar">*</span>
				    </td>
				    <td>
				        <input type="text" name="tel"  placeholder="手机号码" />
				    </td>
				    <td valign="middle">
				        <span class="redstar">*</span>
				    </td>
				    <td>
				        <input type="text" name="email"  placeholder="邮箱地址" />
				    </td>
	            </tr>
	        	<tr>
	        		<td></td>
	            	<td colspan="3">
	            		<input type="hidden" name="type" value="" />
	            		<input type="submit" class="btn" value="确认并投递" />
	            	</td>
	            </tr>
	        </table>
		</form>
    </div><!--/#infoBeforeDeliverResume-->

    <!-- 上传附件简历操作说明-重新上传 -->
    <div id="fileResumeUpload" class="popup">
        <table width="100%">
            <tr>
                <td>
                	<div class="f18 mb10">请上传标准格式的word简历</div>
                </td>
            </tr>
            <tr>
                <td>
                	<div class="f16">
                		操作说明：<br />
                		打开需要上传的文件 - 点击文件另存为 - 选择.docx - 保存
                	</div>
                </td>
            </tr>
        	<tr>
            	<td align="center">
            		<a  class="inline btn" href="#uploadFile" title="上传附件简历">重新上传</a>
            	</td>
            </tr>
        </table>
    </div><!--/#fileResumeUpload-->

    <!-- 上传附件简历操作说明-重新上传 -->
    <div id="fileResumeUploadSize" class="popup">
        <table width="100%">
            <tr>
                <td>
                	<div class="f18 mb10">上传文件大小超出限制</div>
                </td>
            </tr>
            <tr>
                <td>
                	<div class="f16">
                		提示：<br />
                		单个附件不能超过10M，请重新选择附件简历！
                	</div>
                </td>
            </tr>
        	<tr>
            	<td align="center">
            		<a  class="inline btn" href="#uploadFile" title="上传附件简历">重新上传</a>
            	</td>
            </tr>
        </table>
    </div><!--/#deliverResumeConfirm-->

    <!-- 投简历成功前二次确认 -->
    <div id="deliverResumeConfirm" class="popup">
        <table width="100%">
            <tr>
                <td class="msg">
                	<div class="f18">你的简历中：</div>
                	<div class="f16 count"><span class="f18 confirm_field">学历、工作年限、期望工作城市</span>与该职位要求不匹配，确认要投递吗？</div>
                </td>
            </tr>
        	<tr>
            	<td align="center">
            		<input type="hidden" name="type" value="" />
            		<a href="javascript:sendResume(434773,146485,true,true);" class="btn">确认投递</a>
            		<a href="javascript:;" class="btn_s">放弃投递</a>
            		<a href="javascript:;" class="f20 edit_field">修改信息</a>
            	</td>
            </tr>
        </table>
    </div><!--/#deliverResumeConfirm-->

     <!-- 投简历成功 -->
    <div id="deliverResumesSuccess" class="popup" style="width:440px;padding-bottom:10px;">
        <table width="100%">
            <tr class="drawGGJ">
                <td align="center">
                	<p class="font_16 count"></p>
                	<p class="font_16 share dn">邀请好友成功注册拉勾，可提升每日投递量 &nbsp;&nbsp; <a href="http://www.lagou.com/share/invite.html" target="_blank">邀请好友>></a></p>
                </td>
            </tr>
        	<tr class="drawQD">
            	<td align="center"><a href="javascript:top.location.reload();" class="btn_s">确&nbsp;定</a></td>
            </tr>
            <tr class="weixinQR dn">
            	<td>
            		<div class="weixin">
                       	<div class="qr">
                       		<img src="" width="120" height="120" />

                       		<div>[仅限本人使用]</div>
                       	</div>
                       	<div class="qr_text">
						              想知道HR是否看过你的简历？<br />
						              想在微信中收到面试通知？<br />
						    <span><< 微信扫一扫，一并解决</span>
                       	</div>
                   	</div>
            	</td>
            </tr>
        </table>
    </div><!--/#deliverResumesSuccess-->

    <!-- 投递时，一个简历都没有弹窗 -->
	<div id="sendNoResume" class="popup">
	    <table width="100%">
	    	<tr>
	    		<td class="f18 c5" align="center">你还没有可以投递的简历呢</td>
	    	</tr>
	    	<tr>
	    		<td class="c5" align="center">请上传附件简历或填写在线简历后再投递吧~</td>
	    	</tr>
	    	<tr>
	        	<td align="center">
                   <form>
                        <a href="javascript:void(0);" class="btn_addPic">
                        	<span>选择上传文件</span>
                        	<input title="支持word、pdf、ppt、txt、wps格式文件，大小不超过10M" size="3" name="newResume" id="resumeUpload2"
	                        class="filePrew" type="file" onchange="file_check(this,'http://www.lagou.com/nearBy/updateMyResume.json','resumeUpload2')" />
                        </a>
                    </form>
                    <a class="btn" href="http://www.lagou.com/resume/basic.html" target="_blank" rel="nofollow">完善在线简历</a>
	            </td>
	        </tr>
	    </table>
	</div><!--/#sendNoResume-->

    <!-- 没有简历请上传 -->
    <div id="deliverResumesNo" class="popup">
        <table width="100%">
            <tr>
                <td align="center"><p class="font_16">你在拉勾还没有简历，请先上传一份</p></td>
            </tr>
        	<tr>
            	<td align="center">
                    <form>
                        <a href="javascript:void(0);" class="btn_addPic">
                        	<span>选择上传文件</span>
                        	<input title="支持word、pdf、ppt、txt、wps格式文件，大小不超过10M" size="3" name="newResume" id="resumeUpload1"
	                        class="filePrew" type="file" onchange="file_check(this,'http://www.lagou.com/nearBy/updateMyResume.json','resumeUpload1')" />
                        </a>
                    </form>
                </td>
            </tr>
        	<tr>
            	<td align="center">支持word、pdf、ppt、txt、wps格式文件，大小不超过10M</td>
            </tr>
        </table>
    </div><!--/#deliverResumesNo-->

      <!--
    	激活邮箱
		登录邮箱未验证时，
	 	点击上传附件简历、申请职位、订阅职位、发布职位出现该弹窗
	-->
	<div id="activePop" class="popup" style="height:240px;">
       	 <h4>登录邮箱未验证</h4>
       	<p>请验证你的登录邮箱以使用拉勾网的所有功能！       	</p>
       	<p>我们已将验证邮件发送至：<a>catsoul.jassman@gmail.com</a>，请点击邮件内的链接完成验证。</p>
                <p><a href="javascript:void(0)" id="resend">重新发送验证邮件 </a> | <a href="http://www.lagou.com/register.html" target="_blank"> 换个邮箱？ </a>  	</p>
    </div><!--/#activePop-->

     <!--
    	激活邮箱
		验证邮件发送成功弹窗
	-->
    <div id="resend_success" class="popup">
       	 <p>我们已将激活邮件发送至：<a>catsoul.jassman@gmail.com</a>，请点击邮件内的链接完成验证。</p>
       	    </div><!--/#resend_success-->

    <!--地图弹窗-->
        <div id="baiduMap" class="popup">
	        <div id="allmap"></div>
        </div><!--/#baiduMap-->
</div>
<!------------------------------------- end ----------------------------------------->

<script type="text/javascript" src="http://www.lagou.com/js/jobs/job_detail.js?v=1.5.5.6_0825"></script>
<script type="text/javascript" src="http://www.lagou.com/js/count.js?v=1.5.5.6_0825"></script>

<!-- <script type="text/javascript">
var options = {
    	"snsId": "snwb",
    	"uid": "2396316102",
    	"token": "",
    	"openId": "",
    	"appKey": "",
    	"merchantId": "11253",
    	"merchantType": "news",
    	"socialLoginUrl_sina": ctx+"/ologin/auth/sina.html",
    	"socialLoginUrl_qq": "",
    	"weiboId_sina": "3619164344010985",
    	"weiboId_qq": "",
    	"socialAllUrl": ctx+"/user/hbzx.html",
    	"productId": "146485",
    	"productCategory": "",
    	"productName": "Python",
    	"productArea": "北京",
    	"productPrice": "",
    	"productUrl": ctx+"/jobs/146485.html",
    	"productImage": ctx+"/upload/logo/ff8080814503eb8f014517837f2377a4.png",
    	"excerpt": "北京 / 全职 / 10k-15k / 经验1-3年 / 本科及以上 / 职位诱惑 : 弹性工作时间 家庭式午餐 自由的工作环境",
    	"sendContent":"我觉得这个职位不错，你觉得咋样？",
    	"shareContent":"请输入你对此职位的评论",
    	"autoSend": false,
    	"load": true
　　}
</script>
<script src="http://h1.51youwei.com/hbzxrec/js/common/common.js?v=1.5.5.6_0825" type="text/javascript"></script> -->
<script>
$(function(){
	$('#weibolist .cookietxte').text('推荐本职位给好友');
	$(document).bind('cbox_complete', function(){
		hbzxJQ("#gaosutapt .pttui a").trigger("click");
		hbzxJQ("#mepingpt .pttui a").trigger("click");
	});
	$('#cboxOverlay').bind('click',function(){
		top.location.reload();
	});
	$('#colorbox').on('click','#cboxClose',function(){
		if($(this).siblings('#cboxLoadedContent').children('div').attr('id') == 'deliverResumesSuccess' || $(this).siblings('#cboxLoadedContent').children('div').attr('id') == 'uploadFileSuccess'){
			top.location.reload();
		}
	});
			popQR();
	})
</script>

<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=6605604a7755e5d4f1216b19d8dda1b1"></script>
<script type="text/javascript">
		//百度地图API功能
		var sMap = new BMap.Map("smallmap");
		sMap.enableScrollWheelZoom(true);

		if($('#positionLat').val() && $('#positionLng').val()){
			var sPoint = new BMap.Point($('#positionLng').val(),$('#positionLat').val());
			sMap.centerAndZoom(sPoint,12);
			sMap.addOverlay(new BMap.Marker(sPoint));              // 将标注添加到地图中

		}else{
			// 创建地址解析器实例
			var sMyGeo = new BMap.Geocoder();
			// 将地址解析结果显示在地图上,并调整地图视野
			sMyGeo.getPoint("北京市朝阳区郎家园六号院3号楼4层", function(sPoint){
			  if (sPoint) {
				  sMap.centerAndZoom(sPoint, 16);
				  sMap.addOverlay(new BMap.Marker(sPoint));
			  }
			}, "北京");
		}

		/*弹窗大地图*/
		var map = new BMap.Map("allmap");
		map.addControl(new BMap.NavigationControl());
		map.addControl(new BMap.MapTypeControl());
		map.addControl(new BMap.OverviewMapControl());
		map.enableScrollWheelZoom(true);
		// 创建地址解析器实例
		var gc = new BMap.Geocoder();
		$(function(){
			$('#mapPreview').bind('click',function(){
				$.colorbox({inline:true, href:"#baiduMap",title:"公司地址"});
				var address = "北京市朝阳区郎家园六号院3号楼4层";
				var city = "北京";
				var lat = $('#positionLat').val();
				var lng = $('#positionLng').val();
			    map.setCurrentCity(city);
			    map.setZoom(12);
				if(lat && lng){
					var p = new BMap.Point(lng, lat);
					var marker = new BMap.Marker(p);  // 创建标注
					map.addOverlay(marker);              // 将标注添加到地图中
					setTimeout(function(){
						map.centerAndZoom(p, 15);
					},1000);
					map.setCenter(p);
					map.setZoom(15);
					var sContent =
						"<h4 style='margin:0 0 5px 0;padding:0.2em 0'>"+city+"</h4>" +
						"<p style='margin:0;line-height:1.5;font-size:13px;text-indent:2em'>"+address+"</p>" +
						"</div>";
					 var infoWindow = new BMap.InfoWindow(sContent);  // 创建信息窗口对象
				 	//图片加载完毕重绘infowindow
			 		marker.openInfoWindow(infoWindow);
		 		}else{
					gc.getPoint(address, function(point){
						  if (point) {
							  	var p = new BMap.Point(point.lng, point.lat);
								var marker = new BMap.Marker(p);  // 创建标注
								map.addOverlay(marker);              // 将标注添加到地图中
								setTimeout(function(){
									map.centerAndZoom(p, 15);
								},1000);
								map.setZoom(14);
								var sContent =
									"<h4 style='margin:0 0 5px 0;padding:0.2em 0'>"+city+"</h4>" +
									"<p style='margin:0;line-height:1.5;font-size:13px;text-indent:2em'>"+address+"</p>" +
									"</div>";
								 var infoWindow = new BMap.InfoWindow(sContent);  // 创建信息窗口对象
							 	//图片加载完毕重绘infowindow
						 		marker.openInfoWindow(infoWindow);
						  }
					}, city);
				}


			});

		});
</script>
			<div class="clear"></div>
			<input type="hidden" id="resubmitToken" value="d2ef60f2730e44878e72bbc8e4761c58" />
	    	<a id="backtop" title="回到顶部" rel="nofollow"></a>
	    </div><!-- end #container -->
	</div><!-- end #body -->
	<div id="footer">
		<div class="wrapper">
			<a href="http://www.lagou.com/about.html" target="_blank" rel="nofollow">联系我们</a>
		    <a href="http://www.lagou.com/af/zhaopin.html" target="_blank">互联网公司导航</a>
		    <a href="http://e.weibo.com/lagou720" target="_blank" rel="nofollow">拉勾微博</a>
		    <a class="footer_qr" href="javascript:void(0)" rel="nofollow">拉勾微信<i></i></a>
			<div class="copyright">&copy;2013-2014 Lagou <a target="_blank" href="http://www.miitbeian.gov.cn/state/outPortal/loginPortal.action">京ICP备14023790号-2</a></div>
		</div>
	</div>

<script type="text/javascript" src="http://www.lagou.com/js/core.min.js?v=1.5.5.6_0825"></script>
<script type="text/javascript" src="http://www.lagou.com/js/popup.min.js?v=1.5.5.6_0825"></script>
<script type="text/javascript" src="http://www.lagou.com/js/libs/tongji.js?v=1.5.5.6_0825"></script>
<script type="text/javascript" src="http://www.lagou.com/js/libs/analytics.js?v=1.5.5.6_0825"></script>
<script type="text/javascript">
$(function(){
	//判断初始化页面时 是否隐藏提示框以及红点的显示
	if($(".notice_tip").length == 0){
		$("#noticeTip").hide();
		$(".collapsible_menu dt .red").addClass("dn");
	}

	//$('#noticeDot-1').hide();
	$('#noticeTip a.closeNT').click(function(){
		$(this).parent().hide();

				$(".collapsible_menu dt .red").addClass("dn");
	});
	//B端消息绑定单击事件
	});
var index = Math.floor(Math.random() * 2);
var ipArray = new Array('42.62.79.226','42.62.79.227');
var url = "ws://" + ipArray[index] + ":18080/wsServlet?code=434773";
var CallCenter = {
		init:function(url){
			var _websocket = new WebSocket(url);
			_websocket.onopen = function(evt) {
				console.log("Connected to WebSocket server.");
			};
			_websocket.onclose = function(evt) {
				console.log("Disconnected");
			};
			_websocket.onmessage = function(evt) {
				console.log(evt.data);
				var notice = jQuery.parseJSON(evt.data);
				if(notice.status[0] == 0){
					$('#noticeDot-0').hide();
					$('#noticeTip').hide();
					$('#noticeNo').text('').show().parent('a').attr('href',ctx+'/mycenter/delivery.html');
					$('#noticeNoPage').text('').show().parent('a').attr('href',ctx+'/mycenter/delivery.html');
				}else{
					$('#noticeDot-0').show();
					$('#noticeTip strong').text(notice.status[0]);
					$('#noticeTip').show();
					$('#noticeNo').text('('+notice.status[0]+')').show().parent('a').attr('href',ctx+'/mycenter/delivery.html?tag=0&r=0');
					$('#noticeNoPage').text(' ('+notice.status[0]+')').show().parent('a').attr('href',ctx+'/mycenter/delivery.html?tag=0&r=0');
				}
				$('#noticeDot-1').hide();
			};
			_websocket.onerror = function(evt) {
				console.log('Error occured: ' + evt);
			};
		}
};
CallCenter.init(url);
</script>
</body>
</html>
'''


soup = BeautifulSoup(html_doc)
job_html = soup.find('dd', attrs = {'class': 'job_bt'})
# print str(job_html) + '1'
soup = BeautifulSoup(str(job_html))
print soup
print
p=soup.get_text()
print p
print type(p)
# print p_list.p.string