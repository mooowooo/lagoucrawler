__author__ = 'Jux'
# -*- coding: utf-8 -*-
import sys
import urllib2
import os
from bs4 import BeautifulSoup

# logger = Logger()
log_msg = {'logLevel': '', 'logMsg': ''}

doc_local = '/Users/Jux/Desktop/job/'


def SelectItem (item_dict, item_list):
    try:
        result = item_dict[raw_input(item_list)]
    except Exception as e:
        result = item_dict['1']
        print ''.join(['您的输入有误，默认为您选择\'', item_dict['1'], '\''])
        log_msg['logLevel'] = 'ERROR'
        log_msg['logMsg'] = '选择目标城市未找到，默认搜索全国'
        # logger.WriteLog(log_detail = log_msg)
    finally:
        return result


# target_site.join([target_skill,target_city,target_sendtime,...])
dict_city = {'0': '', '1': '全国', '2': '北京', '3': '上海', '4': '广州', '5': '深圳', '6': '杭州', '7': '成都'}
dict_sendtime = {'0': '', '1': '今天', '2': '3天内', '3': '一周内'}
dict_exp = {'0': '', '1': '不限', '2': '1年以下', '3': '1-3年'}
dict_salary = {'0': '', '1': '2k以下', '2': '2K-5K', '3': '5K-10K'}
dict_type = {'0': '', '1': '全职', '2': '兼职', '3': '实习'}

target_site = 'http://www.lagou.com/jobs/'
target_skill = raw_input('技术能力:\n').strip()
target_skill_url = ''.join(['list_', target_skill, '?kd=', target_skill, '&xl='])
target_city = SelectItem(dict_city, '选择目标城市:\n0 默认\n1 全国\n2 北京\n3 上海\n4 广州\n5 深圳\n6 杭州\n7 成都\n')
target_city_url = '&city=' + target_city
target_sendtime_url = '&st=' + SelectItem(dict_sendtime, '选择发布时间:\n0 默认\n1 今天\n2 3天内\n3 一周内\n')
target_exp_url = '&gj=' + SelectItem(dict_exp, '选择最低工作经历:\n0 默认\n1 不限\n2 1年以下\n3 1-3年\n')
target_salary_url = '&yx=' + SelectItem(dict_salary, '选择月薪范围:\n0 默认\n1 2K以下\n2 2K-5K\n3 5K-10K\n')
target_type_url = '&gx=' + SelectItem(dict_type, '选择工作类型:\n0 默认\n1 全职\n2 兼职\n3 实习\n')
target_url = ''.join([target_site, target_skill_url, target_city_url, target_exp_url, target_salary_url,
                      target_sendtime_url, target_type_url])

headers = {'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6'}
# 构造请求头信息，伪装成用户请求而非爬虫请求


# def Grab (target_url, headers):
req = urllib2.Request(target_url, headers = headers)
content = urllib2.urlopen(req).read()  # 获取到的内容为 UTF-8 编码
type = sys.getfilesystemencoding()  # local encode format
html = content.decode("UTF-8").encode(type)  # convert encode format

# def Analy(soup_html, mark):
# soup = BeautifulSoup(html)
# list = soup_html._find_all(mark['name'], attrs = mark['attrs'])

soup = BeautifulSoup(html)
rs_html = soup.findAll("ul", attrs = {"class": "hot_pos reset"})

html = str(rs_html)
soup = BeautifulSoup(html)
# rs_li_html_o = soup.findAll("li", attrs={"class": "odd clearfix"})    # 下一行范围包含此行
rs_li_html = soup.findAll("li", attrs = {"class": "clearfix"})
# print rs_li_html_o
# print rs_li_html
# print len(rs_li_html)

job_detail_link = {}

# 获取首页的所有列表项目
for i in xrange(len(rs_li_html)):
    soup = BeautifulSoup(str(rs_li_html[i]))
    # print soup.a
    link_html = soup.findAll('div', attrs = {"class": "mb10"})
    # print '1: ' + str(link_html[0].a['href']) + '\n'
    # print '2: ' + str(link_html[1].a['href']) + '\n'
    # print link_html[2]
    # print link_html[3]
    sub_html = soup.findAll("em", attrs = {"class": "c7"})
    # # print sub_html
    salary = str(sub_html[0])
    exp = str(sub_html[1])
    degree = str(sub_html[2])
    job_href = str(link_html[0].a['href'])
    job_title = str(link_html[0].a['title'])
    company_href = str(link_html[1].a['href'])
    # print link_html[1].a['title']
    company_name = link_html[1].a['title']
    company_name = company_name.encode('utf-8')

    # sub_html = soup.findAll
    item = {"job_href": job_href, "job_title": job_title, "company_href": company_href, "company_name": company_name,
            "salary": salary, "exp": exp, "degree": degree}
    job_detail_link[i] = item

job_detail = ''
# 分别获取每个列表项的工作详细内容，并写入文件
for i in xrange(len(job_detail_link)):
    req = urllib2.Request(job_detail_link[i]["job_href"], headers = headers)
    content = urllib2.urlopen(req).read()  # 获取到的内容为 UTF-8 编码
    type = sys.getfilesystemencoding()  # local encode format
    html = content.decode("UTF-8").encode(type)  # convert encode format
    soup = BeautifulSoup(html)
    span_list = soup.findAll('span')
    city = span_list[3]
    job_html = soup.find('dd', attrs = {'class': 'job_bt'})
    # company_name = soup.findAll('h2', attrs = {'class': 'fl'})
    # print company_name.get_text()
    company_local = soup.findAll('div')
    company_local = str(company_local[18])
    # print company_local[1].next_sibling
    job_detail += job_detail_link[i]['job_title'] + '\t' + company_local + '\n'
    job_detail += job_detail_link[i]['salary'] + '\t\n'
    job_detail += job_detail_link[i]['exp'] + '\t' + job_detail_link[i]['degree'] + '\n'
    soup = BeautifulSoup(str(job_html))
    t = soup.get_text()
    t = t.encode('utf-8')
    job_detail += t + '\n'
    job_detail += '公司链接: ' + job_detail_link[i]['company_href'] + '\n'
    job_detail += '职位链接: ' + job_detail_link[i]['job_href']+'\n'

    print job_detail
    print doc_local
    doc_name = doc_local +  job_detail_link[i]['job_title']+str(company_name)+ '.txt'
    print doc_name
    # os.mkfifo(doc_name)
    doc_name=unicode(doc_name , "utf8")
    fp = open(doc_name.encode('utf-8'), 'a+')
    print 'ok1 ' + doc_name
    fp.writelines(job_detail)
    print 'ok2'
    fp.close()
    print 'ok3'